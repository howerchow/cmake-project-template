

function(MyFunction FirstArg)

  message("FUNCTION: ${CMAKE_CURRENT_FUNCTION}")
  message("DIR: ${CMAKE_CURRENT_FUNCTION_LIST_DIR}")
  message("FILE: ${CMAKE_CURRENT_FUNCTION_LIST_FILE}")
  message("LINE: ${CMAKE_CURRENT_FUNCTION_LIST_LINE}")

  message("ARGC: ${ARGC}")
  message("ARGV: ${ARGV}")
  message("ARGN: ${ARGN}")

  message("ARGV0: ${ARGV0}")
  message("ARGV1: ${ARGV1}")
  message("ARGV2: ${ARGV2}")

  message("FirstArg: ${${FirstArg}}")

  # set(${FirstArg} "new value" PARENT_SCOPE)

  set(${FirstArg} "new value")

  # message("FirstArg: ${${FirstArg}}")
  # message("OuterVar: ${OuterVar}")

  return(PROPAGATE ${FirstArg})

endfunction()


function(my_install)
  set(options OPTIONAL FAST)
  set(oneValueArgs DESTINATION RENAME)
  set(multiValueArgs TARGETS CONFIGURATIONS)
  cmake_parse_arguments(MY_INSTALL "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})


  # my_install(TARGETS foo bar DESTINATION bin OPTIONAL blub CONFIGURATION)

  message("MY_INSTALL_OPTIONAL  = ${MY_INSTALL_OPTIONAL}")
  message("MY_INSTALL_FAST  = ${MY_INSTALL_FAST}")
  message("MY_INSTALL_DESTINATION  = ${MY_INSTALL_DESTINATION}")
  message("MY_INSTALL_RENAME  = ${MY_INSTALL_RENAME}")
  message("MY_INSTALL_TARGETS  = ${MY_INSTALL_TARGETS}")
  message("MY_INSTALL_CONFIGURATIONS  = ${MY_INSTALL_CONFIGURATIONS}")
  message("MY_INSTALL_UNPARSED_ARGUMENTS  = ${MY_INSTALL_UNPARSED_ARGUMENTS}")
  message("MY_INSTALL_KEYWORDS_MISSING_VALUES  = ${MY_INSTALL_KEYWORDS_MISSING_VALUES}")

endfunction()
