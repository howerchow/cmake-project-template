#include <division.h>
#include <iostream>
#include <map>
#include <print>
#include <string>

int main( int argc, const char* argv[] )
{
    Fraction f;
    // std::cout << HEADER;
#ifdef FOO
    std::println( "{}", FOO );
#endif // FOO

#ifdef BAR
    std::println( "{}", BAR );
#endif // BAR

    // ensure the correct number of parameters are used.
    if ( argc < 3 )
    {
        //  std::cout << USAGE;
        return 1;
    }

    f.numerator = atoll( argv[ 1 ] );
    f.denominator = atoll( argv[ 2 ] );

    Division d { f };
    try
    {
        DivisionResult r = d.divide();

        std::cout << "Division : " << f.numerator << " / " << f.denominator << " = " << r.division << "\n";
        std::cout << "Remainder: " << f.numerator << " % " << f.denominator << " = " << r.remainder << "\n";
    }
    catch ( DivisionByZero )
    {
        std::println( "Can not divide by zero, Homer. Sober up!" );
        // std::cout << "Can not divide by zero, Homer. Sober up!\n";
    }

    // std::map<int, std::string> src { { 1, "one" }, { 2, "two" }, { 3, "buckle my shoe" } };
    // std::map<int, std::string> dst { { 3, "three" } };
    // dst.insert( src.extract( src.find( 1 ) ) ); // Cheap remove and insert of { 1, "one" } from `src` to `dst`.
    // dst.insert( src.extract( 2 ) );             // Cheap remove and insert of { 2, "two" } from `src` to `dst`.
    // dst == { { 1, "one" }, { 2, "two" }, { 3, "three" } };

    return 0;
}
